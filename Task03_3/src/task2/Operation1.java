package task2;

public class Operation1 implements Strategy{
	
	   @Override
	   public void autoPlay() {
		   Aardvark aardvark = new Aardvark(1);
		   aardvark.playerName = "Level 1 Player";
		   aardvark.playGame();
	   }
	   
	}

