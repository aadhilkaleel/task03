package task2;

public class RunCommandPattern {

	public static void main(String[] args) {
		Aardvark ardvark = new Aardvark(1);
		
		PlaceDominoCommand placeDomino = new PlaceDominoCommand(ardvark);
		
		Command_Invoker invoker = new Command_Invoker(placeDomino);
		invoker.doPlaceDomino();
		
		System.out.println("\nUndo the domino placement (Y/N)?");
		String undo = IOLibrary.getString();
		if(undo.equalsIgnoreCase("Y")) {
			invoker.undoPlaceDomino();
		}
	}

}

