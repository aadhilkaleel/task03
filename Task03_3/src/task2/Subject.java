package task2;

import java.util.ArrayList;
import java.util.List;

public class Subject{
	
	private List<Moniter> Moniter = new ArrayList<Moniter>();

	   public void attach(Moniter observer){
		   Moniter.add(observer);		
	   }

	   public void notifyAllObservers(){
	      for (Moniter observer : Moniter) {
	         observer.displayFrame();
	      }
	   } 	
	
}

