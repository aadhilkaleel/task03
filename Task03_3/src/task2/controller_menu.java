package task2;

public class controller_menu {
	private Play_Menu model;
	private View_Menu view;
	
	public controller_menu(Play_Menu model, View_Menu view) {
		this.model = model;
		this.view = view;
	}
	public void setPlayerName(String name) {
		model.setName(name);
	}
	public String getPlayerName() {
		return model.getName();
	}
	public void updateView() {
		view.displayGUIView();
	}
}
