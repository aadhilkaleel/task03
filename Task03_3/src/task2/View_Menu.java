package task2;

public class View_Menu {
	public void displayGUIView() {
		  Subject subject = new Subject();

	      new Welcome_Menu(subject);
	      new Player_Name(subject);
	      new Main_Menu(subject);
	      new Difficulty_Menu(subject);
	      
	      subject.notifyAllObservers();
	}


}
