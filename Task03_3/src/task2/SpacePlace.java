package task2;

public class SpacePlace {
  private SpacePlaceData data = new SpacePlaceData();

public SpacePlace() {
    data.xOrg = 0;
    data.yOrg = 0;
  }

  public SpacePlace(double theta, double phi) {
    super();
    this.data.theta = theta;
    this.data.phi = phi;
  }

  public int getxOrg() {
    return data.xOrg;
  }

  public void setxOrg(int xOrg) {
    this.data.xOrg = xOrg;
  }

  public int getyOrg() {
    return data.yOrg;
  }

  public void setyOrg(int yOrg) {
    this.data.yOrg = yOrg;
  }

  public double getTheta() {
    return data.theta;
  }

  public void setTheta(double theta) {
    this.data.theta = theta;
  }

  public double getPhi() {
    return data.phi;
  }

  public void setPhi(double phi) {
    this.data.phi = phi;
  }
  
}
